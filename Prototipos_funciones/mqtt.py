
import random
from paho.mqtt import client as mqtt_client
from tb_device_mqtt import TBDeviceMqttClient, ProvisionClient, TBPublishInfo
import logging
from datetime import datetime
import sys
import time
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))
class CuentaBilletesSimulator:
    __client = None
    __registered = False
    __host = 'tb.lab-01.howlab.es'
    __port = 1883
    topic_telemetry = "v1/devices/me/telemetry"
    raiz_atributos = "v1/devices/me/attributes"
    # generate client ID with pub prefix randomly
    client_id = f'python-mqtt-{random.randint(0, 1000)}'
    username = 'p8xGAhCPXodaLKX6mCVr'

    @property
    def is_connected(self):
        return self.__client is not None and self.__client.is_connected()

    # password = 'public'


    def register(self, host=__host, port=__port):
        if self.__registered:
            return

        logger.info('Registering...')
        client = TBDeviceMqttClient(host=host, port=port, token=self.token)
        credentials = client.provision(host=host, provision_device_key=self.provisioning_device_key,
                                       provision_device_secret=self.provisioning_secret_key, port=port,
                                       device_name=self.name, access_token=self.token)
        if credentials == self.token:
            self.__host = host
            self.__port = port
            self.__registered = True
            logger.info('Done...')
            return True
        else:
            logger.warning('Error while registering!')
            return False


    def connect(self, host=__host, port=__port):
        self.disconnect()

        client = TBDeviceMqttClient(host=host, port=port, token=self.username)

        logger.info('Connecting...')
        client.connect()
        time.sleep(0.5)
        if not client.is_connected():
            logger.warning('Error while connecting!')
            return False

        self.__client = client
        # client.set_server_side_rpc_request_handler(self.__on_rpc_request)

        # client.request_attributes(client_keys=['total_takings', 'coin_pocket'], callback=self.__on_attributes_updated)
        logger.info('Done...')

        return self.is_connected

    def disconnect(self):
        if self.is_connected:
            logger.info('Disconnecting...')
            self.__client.set_server_side_rpc_request_handler(None)
            self.__client.disconnect()
            logger.info('Done.')
        self.__client = None

    def publish_ind_fajo(self, data):
        nombres_ind_fajo = ['media', 'mediana', 'max', 'min', 'std', 'skew', 'curtosis']
        nombres_ind_billete = ['media', 'mediana', 'max', 'min', 'std', 'skew', 'curtosis']
        nombres_medidas = ['dobles1', 'dobles2', 'v_int', 'v_aux']

        timestamp = datetime.now()
        tb_timestamp = int(timestamp.timestamp() * 1000)
        telemetry = {
            'ts': tb_timestamp
            }
        telemetry['values']={}

        for nombre_medida in nombres_medidas:
            for ind_billete in nombres_ind_billete:
                for ind_fajo in nombres_ind_fajo:
                    valor = data[nombre_medida][ind_billete][ind_fajo]
                    msg = '%s-%s.%s'%(nombre_medida, ind_billete, ind_fajo) #medidas_fajo[cont_medidas]
                    telemetry['values'][msg] = valor
        self.__update_telemetry(telemetry=telemetry)

    def __update_telemetry(self, telemetry):
        if not self.is_connected:
            return False
        logger.debug(f'Sending telemetry {telemetry}...')
        result = self.__client.send_telemetry(telemetry=telemetry)
        try:
            publish_info = result.get()
            if publish_info == TBPublishInfo.TB_ERR_SUCCESS:
                logger.debug('Done sending telemetry')
                return True
            else:
                logger.warning(f'Error while sending telemetry [publish_info={publish_info}]', )
                return False
        except Exception as e:
            print(e)

class Mqtt_handler:
    client = None
    broker = 'tb.lab-01.howlab.es'
    port = 1883
    topic_telemetry = "v1/devices/me/telemetry"
    raiz_atributos = "v1/devices/me/attributes"
    # generate client ID with pub prefix randomly
    client_id = f'python-mqtt-{random.randint(0, 1000)}'
    username = 'p8xGAhCPXodaLKX6mCVr'
    # password = 'public'

    def connect_mqtt(self):
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                print("Connected to MQTT Broker!")
            else:
                print("Failed to connect, return code %d\n", rc)

        self.client = TBDeviceMqttClient(self.client_id)
        self.client.username_pw_set(self.username)
        self.client.on_connect = on_connect
        self.client.connect(self.broker, self.port)

    def disconnect_mqtt(self):
        self.client.disconnect()

    def publish(self, topic, msg):
        result = self.client.publish(topic, msg)
        # result: [0, 1]
        status = result[0]
        if status == 0:
            print(f"Send `{msg}` to topic `{topic}`")
        else:
            print(f"Failed to send message to topic {topic}")

    def publish_ind_fajo(self, data):
        nombres_ind_fajo = ['media', 'mediana', 'max', 'min', 'std', 'skew', 'curtosis']
        nombres_ind_billete = ['media', 'mediana', 'max', 'min', 'std', 'skew', 'curtosis']
        nombres_medidas = ['dobles1', 'dobles2', 'v_int', 'v_aux']

        for nombre_medida in nombres_medidas:
            for ind_billete in nombres_ind_billete:
                for ind_fajo in nombres_ind_fajo:
                    valor = data[nombre_medida][ind_billete][ind_fajo]
                    msg = '{"%s-%s.%s":%s}'%(nombre_medida, ind_billete, ind_fajo, valor) #medidas_fajo[cont_medidas]
                    result = self.client.publish(self.topic_telemetry, msg)
                    status = result[0]
                    if status == 0:
                        print(f"Send `{msg}` to topic `{self.topic_telemetry}`")
                    else:
                        print(f"Failed to send message to topic {self.topic_telemetry}")

# def run():
#     client = connect_mqtt()
#     client.loop_start()
#     publish_ind_fajo(client)
#
# if __name__ == '__main__':
#     run()