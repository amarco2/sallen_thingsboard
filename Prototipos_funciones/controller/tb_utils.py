from datetime import datetime


def get_tb_timestamp(timestamp=False):
    if not timestamp or type(timestamp) is not datetime:
        timestamp = datetime.now()

    tb_timestamp = int(timestamp.timestamp() * 1000)
    return tb_timestamp


def get_tb_telemetry_json(values: dict, timestamp=False):
    tb_timestamp = get_tb_timestamp(timestamp)
    telemetry_json = {
        'ts': tb_timestamp,
        'values': values
    }
    return telemetry_json
