from generic_tb_device import GenericTBDevice
from datetime import datetime


class Counter(GenericTBDevice):

    def __init__(self, name, label, host=None, port=None, access_token=None, provision_key=None, provision_secret=None):
        # provision_key = '982a9fh13z8nfl00o5hg'
        # provision_secret = 'ikaheqtvdtnc8x18zub0'
        if not access_token:
            access_token = f'{name}_ACCESS_TOKEN'  # preestablecemos el token que usaremos para registrar el dispositivo
        super().__init__(name=name, label=label, host=host, port=port, access_token=access_token,
                         provision_key=provision_key, provision_secret=provision_secret)

    def register_bundle_data(self, bundle_id, data, timestamp: datetime):
        values = {'bundle_id': bundle_id}
        values.update(data)
        return self.update_telemetry(values=values, timestamp=timestamp)
